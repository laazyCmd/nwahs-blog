const config = {
	mode: "jit",
	purge: [
		"./src/**/*.{html,js,svelte,ts,tsx}",
	],
	theme: {
		extend: {},
	},
	plugins: [],
	darkMode: "class",
};

module.exports = config;
